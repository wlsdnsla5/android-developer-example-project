package jin.portfolio.basic_application

import android.app.Application
import org.koin.core.context.startKoin
import org.koin.core.module.Module

class BasicApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin { modules(getKoinModuleList()) }
    }

    private fun getKoinModuleList() = listOf<Module>()
}