package jin.portfolio.basic_application.lib_android.fragment

import jin.portfolio.basic_application.lib_android.keyboard.KeyboardVisibility
import jin.portfolio.basic_application.lib_android.toolbar.StandardToolbarState
import jin.portfolio.basic_application.lib_android.toolbar.ToolbarNavigation
import jin.portfolio.basic_application.lib_android.toolbar.ToolbarOption
import jin.portfolio.basic_application.lib_android.toolbar.ToolbarState

sealed class FragmentViewState {
    abstract val isNavigationEnabled: Boolean
    abstract val toolbarState: ToolbarState
    abstract val keyboardVisibility: KeyboardVisibility
}

data class StandardFragmentViewState(
    override val toolbarState: ToolbarState,
    override val isNavigationEnabled: Boolean = true
) : FragmentViewState() {
    constructor(
        toolbarNavigation: ToolbarNavigation,
        toolbarOption: ToolbarOption,
        titleId: Int?,
        backgroundColorId: Int?
    ) : this(
        toolbarState = StandardToolbarState(
            toolbarNavigation = toolbarNavigation,
            toolbarOption = toolbarOption,
            titleId = titleId,
            backgroundColorId = backgroundColorId
        )
    )

    override val keyboardVisibility: KeyboardVisibility = KeyboardVisibility.HIDE
}
