package jin.portfolio.basic_application.lib_android.toolbar

sealed class ToolbarState {
    abstract val toolbarNavigation: ToolbarNavigation
    abstract val toolbarOption: ToolbarOption
    abstract val isWindowMode: Boolean
    abstract val titleId: Int?
    abstract val backgroundColorId: Int?
}

data class StandardToolbarState(
    override val toolbarNavigation: ToolbarNavigation = ToolbarNavigation.NONE,
    override val toolbarOption: ToolbarOption = ToolbarOption.NONE,
    override val titleId: Int? = null,
    override val backgroundColorId: Int? = null
) : ToolbarState() {
    override val isWindowMode: Boolean = false
}

data class DialogToolbarState(override val titleId: Int? = null) : ToolbarState() {
    override val toolbarNavigation: ToolbarNavigation = ToolbarNavigation.NONE
    override val toolbarOption: ToolbarOption = ToolbarOption.NONE
    override val backgroundColorId: Int? = null
    override val isWindowMode: Boolean = true
}

object NoActionToolbarState : ToolbarState() {
    override val toolbarNavigation: ToolbarNavigation = ToolbarNavigation.NONE
    override val toolbarOption: ToolbarOption = ToolbarOption.NONE
    override val titleId: Int? = null
    override val backgroundColorId: Int? = null
    override val isWindowMode: Boolean = false
}