package jin.portfolio.basic_application.lib_android.fragment

import androidx.lifecycle.*
import autodispose2.lifecycle.CorrespondingEventsFunction
import autodispose2.lifecycle.LifecycleEndedException
import autodispose2.lifecycle.LifecycleScopeProvider
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.BehaviorSubject

open class ViewLifecycleOwner :
    LifecycleOwner,
    LifecycleObserver,
    LifecycleScopeProvider<FragmentEvents> {

    private val lifecycleEventsSubject by lazy {
        BehaviorSubject.create<FragmentEvents>().apply { onNext(FragmentEvents.VIEW_CREATED) }
    }
    private val lifecycleRegistry =
        LifecycleRegistry(this).apply { addObserver(this@ViewLifecycleOwner) }

    override fun getLifecycle() = lifecycleRegistry

    override fun lifecycle(): Observable<FragmentEvents> = lifecycleEventsSubject.hide()

    override fun correspondingEvents(): CorrespondingEventsFunction<FragmentEvents> =
        CORRESPONDING_EVENTS
    override fun peekLifecycle(): FragmentEvents = lifecycleEventsSubject.value

    fun onDestroy() = lifecycleEventsSubject.onNext(FragmentEvents.VIEW_DESTROYED)

    private companion object {
        private val CORRESPONDING_EVENTS = CorrespondingEventsFunction<FragmentEvents> {
            when (it) {
                FragmentEvents.VIEW_CREATED -> FragmentEvents.VIEW_DESTROYED
                else -> throw LifecycleEndedException("Cannot use VM lifecycle after cleared")
            }
        }
    }
}