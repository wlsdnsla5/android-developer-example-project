package jin.portfolio.basic_application.lib_android.data

class Bytes(val value: ByteArray) {
    val hexString by lazy { value.toHexString() }
}

internal fun ByteArray.toHexString() = bytesToHex(this)

private val HEX = "0123456789ABCDEF".toCharArray()

private fun bytesToHex(bytes: ByteArray): String {
    val hexChars = CharArray(bytes.size * 2)
    for (index in bytes.indices) {
        val v = bytes[index].toInt() and 0xFF
        hexChars[index * 2] = HEX[v.ushr(4)]
        hexChars[index * 2 + 1] = HEX[v and 0x0F]
    }

    return String(hexChars)
}
