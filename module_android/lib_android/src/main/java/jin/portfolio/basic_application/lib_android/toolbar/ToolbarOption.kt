package jin.portfolio.basic_application.lib_android.toolbar

enum class ToolbarOption {
    SETTINGS, MORE, SEARCH, NONE
}