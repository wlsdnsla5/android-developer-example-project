package jin.portfolio.basic_application.lib_android.navigator

import android.view.View

data class FragmentNavigationParams(
    val expandToolbarWithAnimation: Boolean = true,
    val sharedElement: SharedElement?
) {
}

data class SharedElement(val transitionName: String, val sharedView: View)