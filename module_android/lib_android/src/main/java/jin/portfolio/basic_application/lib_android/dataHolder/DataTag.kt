package jin.portfolio.basic_application.lib_android.dataHolder

object DataTag {
    const val DISTURBANCE = "DISTURBANCE"
    const val SECURITY = "SECURITY"
    const val LANGUAGE = "LANGUAGE"
    const val BLUETOOTH = "BLUETOOTH"
    const val AIRPLANE_MODE = "AIRPLANE_MODE"
    const val WIFI_MODE = "WIFI_MODE"
    const val ORIENTATION = "ORIENTATION"
    const val LOG_IN = "ORIENTATION"
    const val ADMIN = "ORIENTATION"
}