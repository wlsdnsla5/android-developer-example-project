package jin.portfolio.basic_application.lib_android.keyboard

enum class KeyboardType {
    NUMBER, NUMBER_DECIMAL, TEXT
}