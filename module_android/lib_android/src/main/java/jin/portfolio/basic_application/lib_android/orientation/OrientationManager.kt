package jin.portfolio.basic_application.lib_android.orientation

import androidx.lifecycle.MutableLiveData

interface OrientationManager {
    fun getOrientation(): MutableLiveData<Orientation>
    fun changeOrientation(orientation: Orientation)
}