package jin.portfolio.basic_application.lib_android.data

import java.lang.IndexOutOfBoundsException

class SecretString(val value: CharArray) : CharSequence {

    constructor(string: String) : this(string.toCharArray())

    fun substring(startIndex: Int, endIndex: Int) = subSequence(startIndex, endIndex)

    override val length: Int get() = value.size

    override fun get(index: Int) = value[index.takeIfCorrectIndex()]

    override fun subSequence(startIndex: Int, endIndex: Int) =
        value.copyOfCorrectRange(getCorrectIndexPair(startIndex, endIndex))
            ?.let { SecretString(it) } ?: throw IndexOutOfBoundsException(
            "Incorrect indexes[start($startIndex) - end($endIndex)] for SensitiveString($length)"
        )

    override fun toString() =
        value.first().toString() + "*".repeat(length - 2) + value.last().toString()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return value.contentEquals((other as SensitiveString).value)
    }

    override fun hashCode() = value.contentHashCode()

    private fun CharArray.copyOfCorrectRange(pair: Pair<Int, Int>?) = pair?.let {
        takeIf { pair.first <= pair.second }?.copyOfRange(pair.first, pair.second)
    }

    private fun getCorrectIndexPair(startIndex: Int, endIndex: Int) = try {
        Pair(startIndex.takeIfCorrectIndex(), endIndex.takeIfCorrectIndex())
    } catch (exception: IndexOutOfBoundsException) {
        null
    }

    private fun Int.takeIfCorrectIndex() = this.takeIf { it in 0..length }
        ?: throw IndexOutOfBoundsException("Incorrect index[$this] for SensitiveString($length)")
}

fun String.toSecretString() = SecretString(this)

fun String?.toSecretStringOrEmpty() = this?.let(::SecretString) ?: SecretString("")
