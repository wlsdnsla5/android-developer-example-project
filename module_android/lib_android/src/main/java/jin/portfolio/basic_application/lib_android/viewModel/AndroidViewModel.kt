package jin.portfolio.basic_application.lib_android.viewModel

import androidx.lifecycle.ViewModel
import autodispose2.lifecycle.CorrespondingEventsFunction
import autodispose2.lifecycle.LifecycleEndedException
import autodispose2.lifecycle.LifecycleScopeProvider
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.BehaviorSubject
/**
 * writer : Jin woo lee
 * This VM is LifecycleScope integration implementation.
 * Use to view or other concurrent implementation will be useful
 * */
abstract class AndroidViewModel : ViewModel(), LifecycleScopeProvider<ViewModelLifecycleEvent> {
    private val lifecycleEventSubject by lazy {
        BehaviorSubject.create<ViewModelLifecycleEvent>()
            .apply { onNext(ViewModelLifecycleEvent.ACTIVE) }
    }
    internal var isInitialized = false

    override fun onCleared() {
        super.onCleared()
        lifecycleEventSubject.onNext(ViewModelLifecycleEvent.INACTIVE)
    }

    override fun lifecycle(): Observable<ViewModelLifecycleEvent> = lifecycleEventSubject.hide()

    override fun correspondingEvents() = CorrespondingEventsFunction<ViewModelLifecycleEvent> {
        when (it) {
            ViewModelLifecycleEvent.ACTIVE -> ViewModelLifecycleEvent.INACTIVE
            else -> throw LifecycleEndedException("cannot use VM lifecycle")
        }
    }

    override fun peekLifecycle(): ViewModelLifecycleEvent = lifecycleEventSubject.value
}

enum class ViewModelLifecycleEvent {
    ACTIVE, INACTIVE
}