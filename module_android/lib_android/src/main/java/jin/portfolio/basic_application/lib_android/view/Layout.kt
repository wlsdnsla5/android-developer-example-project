package jin.portfolio.basic_application.lib_android.view

import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import kotlinx.android.parcel.Parcelize

@Suppress("UNCHECKED_CAST")
@Parcelize
data class Layout(@LayoutRes val id: Int, val shouldAttachToRoot: Boolean = false) : Parcelable {

    fun <ViewType : View> inflate(
        layoutInflater: LayoutInflater,
        viewGroup: ViewGroup? = null
    ): ViewType = layoutInflater.inflate(id, viewGroup, shouldAttachToRoot) as ViewType
}