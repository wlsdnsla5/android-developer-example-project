package jin.portfolio.basic_application.lib_android.extension

import org.threeten.bp.LocalDateTime

fun LocalDateTime.toLogString() = "$year:$month:$dayOfMonth, $hour:$minute:$second"
