package jin.portfolio.basic_application.lib_android.fragment

import androidx.lifecycle.MutableLiveData
import jin.portfolio.basic_application.lib_android.keyboard.KeyboardType
import jin.portfolio.basic_application.lib_android.keyboard.KeyboardVisibility
import jin.portfolio.basic_application.lib_android.orientation.Orientation
import jin.portfolio.basic_application.lib_android.viewModel.AndroidViewModel

class AndroidFragmentViewModel : AndroidViewModel() {
    val keyboardVisibilityLiveData = MutableLiveData<KeyboardVisibility>()

    val keyboardTypeLiveData = MutableLiveData<KeyboardType>()

    val orientationLiveData = MutableLiveData<Orientation>()

    // TODO(Think about what shouldn't disturb) e.g. notification, close fragment
    val isDoNotDisturbMode = MutableLiveData<Boolean>(false)

    val appBarLayoutCollapsingModeLiveData = MutableLiveData<Boolean>()

    val fragmentTransitionStateLiveData = MutableLiveData<Boolean>()

    val toolbarStatusLiveData = MutableLiveData<KeyboardType>()

    val navigationBarStatusLiveData = MutableLiveData<KeyboardType>()

}