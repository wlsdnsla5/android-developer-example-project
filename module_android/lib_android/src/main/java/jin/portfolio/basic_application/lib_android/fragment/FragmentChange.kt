package jin.portfolio.basic_application.lib_android.fragment

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FragmentChange(
    val fragment: AndroidFragment,
    val fragmentArgs: Parcelable?,
    val fragmentChangeBackStack: FragmentChangeBackStack = FragmentChangeBackStack.REPLACE
)