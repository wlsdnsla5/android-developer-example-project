package jin.portfolio.basic_application.lib_android.keyboard

enum class KeyboardVisibility {
    SHOW, HIDE, DO_NOT_CHANGE
}