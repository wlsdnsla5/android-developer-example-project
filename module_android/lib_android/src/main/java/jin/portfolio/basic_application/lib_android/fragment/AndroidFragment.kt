package jin.portfolio.basic_application.lib_android.fragment

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import jin.portfolio.basic_application.domain.copycat.Copycat
import jin.portfolio.basic_application.lib_android.keyboard.KeyboardManager
import jin.portfolio.basic_application.lib_android.keyboard.KeyboardType
import jin.portfolio.basic_application.lib_android.keyboard.KeyboardVisibility
import jin.portfolio.basic_application.lib_android.orientation.Orientation
import jin.portfolio.basic_application.lib_android.orientation.OrientationManager
import jin.portfolio.basic_application.lib_android.view.Layout
import org.koin.android.ext.android.getKoin
import org.koin.androidx.viewmodel.ext.android.viewModel

sealed class AndroidFragment : Fragment(),
    KeyboardManager,
    OrientationManager
{
    private val viewModel by viewModel<AndroidFragmentViewModel>()
    private var viewLifecycleOwner: ViewLifecycleOwner? = null
    private var koinModules: List<String>? = null

    open val fragmentMode: FragmentMode = FragmentMode.PUBLIC_PROCESS

    abstract var args: Parcelable?

    abstract fun onCreateView(): Layout

    open fun onViewCreated(context: Context, viewLifecycleOwner: ViewLifecycleOwner) {
        getKeyboardVisibility().observe(viewLifecycleOwner) {
            if (it == KeyboardVisibility.HIDE) {
                hideKeyboard()
                changeKeyboardVisibility(KeyboardVisibility.DO_NOT_CHANGE)
            }
        }

        getKeyboardType().observe(viewLifecycleOwner) {
            // todo implement later
        }

        getOrientation().observe(viewLifecycleOwner) {
            // todo implement later
        }
        this.activity
    }

    fun scopeWith(vararg koinModuleNames: String) {
        this.koinModules = listOf(*koinModuleNames)
        activity?.resources?.configuration?.orientation
    }

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = onCreateView().inflate(inflater, container)

    final override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewLifecycleOwner = ViewLifecycleOwner()
            .apply { lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_CREATE) }
        this.viewLifecycleOwner = viewLifecycleOwner

        try {
            onViewCreated(requireContext(), viewLifecycleOwner)
        } catch (e: Exception) {
            Copycat.e(e)
        }
    }

    open fun handleFragmentResult(fragmentResult: FragmentResult) {
        TODO("add super method for FragmentRequestCode")
    }

    // keyboard manager
    override fun getKeyboardVisibility() = viewModel.keyboardVisibilityLiveData

    override fun changeKeyboardVisibility(visibility: KeyboardVisibility) {
        viewModel.keyboardVisibilityLiveData.value = visibility
    }

    override fun getKeyboardType() = viewModel.keyboardTypeLiveData

    override fun changeKeyboardType(inputType: KeyboardType) {
        viewModel.keyboardTypeLiveData.value = inputType
    }

    // orientation manager
    override fun getOrientation() = viewModel.orientationLiveData

    override fun changeOrientation(orientation: Orientation) {
        viewModel.orientationLiveData.value = orientation
    }

    override fun onStart() {
        super.onStart()
        viewLifecycleOwner?.lifecycle?.handleLifecycleEvent(Lifecycle.Event.ON_START)
    }

    override fun onResume() {
        super.onResume()
        viewLifecycleOwner?.lifecycle?.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    override fun onPause() {
        super.onPause()
        viewLifecycleOwner?.lifecycle?.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    }

    override fun onStop() {
        super.onStop()
        viewLifecycleOwner?.lifecycle?.handleLifecycleEvent(Lifecycle.Event.ON_STOP)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewLifecycleOwner?.onDestroy()
        viewLifecycleOwner = null
        releaseKoinModules()
    }

    private fun releaseKoinModules() {
        koinModules?.forEach { getKoin().deleteScope(it) }
    }

    private fun hideKeyboard() {
        getInputmethodManager()?.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    private fun getInputmethodManager() =
        context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
}