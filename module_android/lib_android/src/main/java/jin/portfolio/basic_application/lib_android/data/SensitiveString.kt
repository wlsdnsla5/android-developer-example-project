package jin.portfolio.basic_application.lib_android.data

import java.lang.IndexOutOfBoundsException

class SensitiveString(val value: CharArray) : CharSequence {

    constructor(string: String) : this(string.toCharArray())

    override val length: Int get() = value.size

    override fun get(index: Int) = value[index.takeIfCorrectIndex()]

    override fun subSequence(startIndex: Int, endIndex: Int) =
        value.copyOfCorrectRange(getCorrectIndexPair(startIndex, endIndex))
            ?.let { SensitiveString(it) } ?: throw IndexOutOfBoundsException(
            "Incorrect indexes[start($startIndex) - end($endIndex)] for SensitiveString($length)"
        )

    override fun toString(): String = String(value)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return value.contentEquals((other as SensitiveString).value)
    }

    override fun hashCode() = value.contentHashCode()

    fun substring(startIndex: Int, endIndex: Int) = subSequence(startIndex, endIndex)

    private fun CharArray.copyOfCorrectRange(pair: Pair<Int, Int>?) = pair?.let {
        takeIf { pair.first <= pair.second }?.copyOfRange(pair.first, pair.second)
    }

    private fun getCorrectIndexPair(startIndex: Int, endIndex: Int) = try {
        Pair(startIndex.takeIfCorrectIndex(), endIndex.takeIfCorrectIndex())
    } catch (exception: IndexOutOfBoundsException) {
        null
    }

    private fun Int.takeIfCorrectIndex() = this.takeIf { it in 0..length }
        ?: throw IndexOutOfBoundsException("Incorrect index[$this] for SensitiveString($length)")
}

fun String.toSensitiveString() = SensitiveString(this)

fun String?.toSensitiveStringOrEmpty() = this?.let(::SensitiveString) ?: SensitiveString("")
