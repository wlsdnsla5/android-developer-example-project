package jin.portfolio.basic_application.lib_android.credential

data class SensitiveDataKey(val value: String?) {

    companion object {
        private val emptyKey = SensitiveDataKey("EMPTY")

        @JvmStatic
        fun empty() = emptyKey
    }
}