package jin.portfolio.basic_application.lib_android.credential

import org.koin.core.component.KoinApiExtension

@KoinApiExtension
class SensitiveDataKeyProvider(
    val sensitiveDataKeyGenerator: SensitiveDataKeyGenerator
) {
}