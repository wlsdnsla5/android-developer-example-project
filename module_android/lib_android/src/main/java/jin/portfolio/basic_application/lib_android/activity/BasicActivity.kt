package jin.portfolio.basic_application.lib_android.activity

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import jin.portfolio.basic_application.domain.copycat.Copycat
import jin.portfolio.basic_application.lib_android.R
import jin.portfolio.basic_application.lib_android.extension.handleFragmentResult
import jin.portfolio.basic_application.lib_android.extension.removeBackStack
import jin.portfolio.basic_application.lib_android.fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Basic Activity
 *
 * Fragment change management
 * credential
 *
 * */

abstract class BasicActivity : AppCompatActivity(), BindFragmentListener {
    private val viewModel by viewModel<BasicActivityViewModel>()
    private var viewLifecycleOwner: ViewLifecycleOwner? = null

    fun handleFragmentResult(fragmentResult: FragmentResult? = null) {
        viewModel.fragmentResultLiveData.value = fragmentResult
    }

    open fun onCreate(viewLifecycleOwner: ViewLifecycleOwner) {
        viewModel.fragmentChangeLiveData.observe(viewLifecycleOwner) {
            handleFragmentChange(it)
        }

        viewModel.fragmentResultLiveData.observe(viewLifecycleOwner) {
            supportFragmentManager.handleFragmentResult(it)
        }

        viewModel.liveInformationMessage.observe(viewLifecycleOwner) {
            handleLiveInformationMessage(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView(R.layout.activty_basic)
        val viewLifecycleOwner = ViewLifecycleOwner()
            .apply { lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_CREATE) }
        this.viewLifecycleOwner = viewLifecycleOwner
        try {
            onCreate(viewLifecycleOwner)
        } catch (e: Exception) {
            Copycat.e(e)
        }
    }

    override fun bindAndroidFragment(
        fragment: AndroidFragment,
        viewLifecycleOwner: ViewLifecycleOwner
    ) {

    }

    override fun onStart() {
        super.onStart()
        viewLifecycleOwner?.lifecycle?.handleLifecycleEvent(Lifecycle.Event.ON_START)
    }

    override fun onResume() {
        super.onResume()
        viewLifecycleOwner?.lifecycle?.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    override fun onPause() {
        super.onPause()
        viewLifecycleOwner?.lifecycle?.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    }

    override fun onStop() {
        super.onStop()
        viewLifecycleOwner?.lifecycle?.handleLifecycleEvent(Lifecycle.Event.ON_STOP)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewLifecycleOwner?.onDestroy()
        viewLifecycleOwner = null
    }

    private fun handleFragmentChange(fragmentChange: FragmentChange) {
        viewModel.changeFragment(fragmentChange)
        supportFragmentManager
            .removeBackStack(fragmentChange.fragmentChangeBackStack)
            .replace(R.id.activity_fragment_layout, fragmentChange.fragment)
            .commit()
    }

    private fun handleLiveInformationMessage(message: String?) {
        // Todo add information message box when message received and add fragment (e.g. Message style , Chat style etc)
    }

}