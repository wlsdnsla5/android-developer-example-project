package jin.portfolio.basic_application.lib_android.extension

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import jin.portfolio.basic_application.lib_android.activity.BasicActivity
import jin.portfolio.basic_application.lib_android.fragment.AndroidFragment
import jin.portfolio.basic_application.lib_android.fragment.FragmentChangeBackStack
import jin.portfolio.basic_application.lib_android.fragment.FragmentResult


fun AndroidFragment.finish(result: FragmentResult) {
    (activity as? BasicActivity)?.handleFragmentResult(result)
    finish()
}

fun AndroidFragment.finish() {
    (activity as? BasicActivity)?.handleFragmentResult()
    onStop()
}

fun FragmentManager.handleFragmentResult(fragmentResult: FragmentResult) {
    fragments.forEach { it.applyFragmentResult(fragmentResult) }
}

fun FragmentManager.removeBackStack(fragmentChangeBackStack: FragmentChangeBackStack) =
    beginTransaction().let { transaction ->
        when (fragmentChangeBackStack) {
            FragmentChangeBackStack.CLEAR_ALL -> transaction.apply { fragments.forEach { remove(it) } }
            FragmentChangeBackStack.CLEAR_CURRENT -> fragments[0]?.let { transaction.remove(it) }
            else -> transaction
        } ?: transaction
    }

private fun Fragment.applyFragmentResult(fragmentResult: FragmentResult) {
    (this as? AndroidFragment)?.handleFragmentResult(fragmentResult)
}

// TODO
//internal fun FragmentManager.handleFragmentChange(
//    @IdRes layoutId: Int,
//    fragmentChange: FragmentChange
//) {
//    val contentFragment = getContentFragment()
//    val isCurrentAndNewFragmentSameClass = contentFragment?.javaClass == fragmentChange.newFragment.javaClass
//    val shouldRemoveCurrentFragment = fragmentChange.removeCurrentFragment ||
//           (fragmentChange.removeCurrentFragmentIfSameClass && isCurrentAndNewFragmentSameClass)
//    val shouldClearBackStack = contentFragment == null ||
//           fragmentChange.clearBackStack ||
//           (backStackEntryCount == 0 && shouldRemoveCurrentFragment)
//
//    val contentFragmentTag =
//        when {
//            shouldClearBackStack -> {
//                clearFragmentPopStack()
//                getRootContentFragmentTag()
//            }
//            shouldRemoveCurrentFragment -> {
//                contentFragment?.let { it.duringFragmentTransactionLiveData.value = true }
//                val currentFragmentTag = getCurrentContentFragmentTag()
//                popBackStack()
//                currentFragmentTag
//            }
//            else -> getNextContentFragmentTag()
//        }
//
//    beginTransaction()
//        .replace(contentLayoutId, fragmentChange.newFragment, contentFragmentTag)
//        .runIfTrue(!shouldClearBackStack) { addToBackStack(contentFragmentTag) }
//        .runIfNotNull(fragmentChange.sharedElement) {
//            addSharedElementFixed(it.sharedView, it.transitionName)
//        }
//        .commit()
//}
//
//fun FragmentManager.getContentFragment(tag: String = getCurrentContentFragmentTag()): ContentFragment? {
//    return findFragmentByTag(tag) as ContentFragment?
//}
//
//internal fun FragmentManager.getNavigationBackStackState(): NavigationBackStackState {
//    val navigationBackStackItem = ArrayList<NavigationBackStackItem>()
//    for (i in 0..backStackEntryCount) {
//        getContentFragment(createContentFragmentTag(i))?.let { navigationBackStackItem.add(NavigationBackStackItem(it)) }
//    }
//    return NavigationBackStackState(navigationBackStackItem)
//}
//
//// This is workaround to add shared element to transition due to Android bug which caused application crash
//// https://issuetracker.google.com/issues/37135904
//@Suppress("DEPRECATION")
//private fun FragmentTransaction.addSharedElementFixed(
//    sharedElement: View,
//    transitionName: String
//): FragmentTransaction {
//    return setAllowOptimization(true).addSharedElement(sharedElement, transitionName)
//}
//
//private fun getRootContentFragmentTag(): String {
//    return createContentFragmentTag(0)
//}
//
//private fun FragmentManager.getCurrentContentFragmentTag(): String {
//    return createContentFragmentTag(backStackEntryCount)
//}
//
//private fun FragmentManager.getNextContentFragmentTag(): String {
//    return createContentFragmentTag(backStackEntryCount + 1)
//}
//
//private fun createContentFragmentTag(numberOfBackStack: Int): String {
//    return "${ContentFragment::class.java.simpleName}_$numberOfBackStack"
//}
//
//private fun FragmentManager.clearFragmentPopStack() {
//    for (i in 0 until backStackEntryCount) {
//        popBackStack()
//    }
//}
