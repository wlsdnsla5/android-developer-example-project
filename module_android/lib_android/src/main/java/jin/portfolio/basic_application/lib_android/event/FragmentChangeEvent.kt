package jin.portfolio.basic_application.lib_android.event

import jin.portfolio.basic_application.lib_android.extension.className
import jin.portfolio.basic_application.lib_android.extension.toLogString
import jin.portfolio.basic_application.lib_android.fragment.FragmentChange
import jin.portfolio.basic_application.lib_android.fragment.FragmentChangeBackStack
import jin.portfolio.basic_application.lib_android.fragment.FragmentMode
import jin.portfolio.basic_application.lib_android.fragment.FragmentResult
import org.threeten.bp.LocalDateTime

/**
 * FragmentChangeEvent
 *
 * To log all the behavior from application
 *  Opened fragment name
 *  Opened Date and Time
 *
 *  connected to navigator
 * */
data class FragmentChangeEvent(
    val fragmentName: String,
    val startTime: LocalDateTime,
    val fragmentChangeBackStack: FragmentChangeBackStack = FragmentChangeBackStack.REPLACE,
    var fragmentResult: FragmentResult? = null,
    val endTime: LocalDateTime? = null
) {
    override fun toString() = "Fragment $fragmentName " +
           "[start: ${startTime.toLogString()} - ${endTime?.toLogString() ?: ""}] " +
           "with ClearBackStack($fragmentChangeBackStack), " +
           "result : ${(fragmentResult?.toString() ?: "")}"

    companion object {
        internal fun create(fragmentChange: FragmentChange) = fragmentChange.fragment
            .takeIf { it.fragmentMode != FragmentMode.HIDDEN_PROCESS }?.className?.let {
                FragmentChangeEvent(it, LocalDateTime.now(), fragmentChange.fragmentChangeBackStack)
            }
    }
}