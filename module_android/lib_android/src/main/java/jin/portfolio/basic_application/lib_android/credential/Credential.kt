package jin.portfolio.basic_application.lib_android.credential

import jin.portfolio.basic_application.lib_android.data.SensitiveString
import org.threeten.bp.LocalDateTime

data class Credential(
    val userId: SensitiveString,
    val userName: SensitiveString? = null,
    val startedAt: LocalDateTime? = null,
    val updatedAt: LocalDateTime? = null
) {
    companion object {
        val anonymousUserCredential = Credential(SensitiveString("Anonymous"))
    }

    fun isLoggedIn() = this != anonymousUserCredential
}