package jin.portfolio.basic_application.lib_android.fragment

interface BindFragmentListener {

    fun bindAndroidFragment(fragment: AndroidFragment, viewLifecycleOwner: ViewLifecycleOwner)

    // TODO Add more functionality
    // add menu
    // add status bar
}