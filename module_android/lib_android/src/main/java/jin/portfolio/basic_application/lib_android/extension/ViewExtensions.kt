package jin.portfolio.basic_application.lib_android.extension

import android.view.View

fun View.visibleOrGone(flag: Boolean) {
    visibility = View.VISIBLE.takeIf { flag } ?: View.GONE
}

fun View.visibleOrInvisible(flag: Boolean) {
    visibility = View.VISIBLE.takeIf { flag } ?: View.INVISIBLE
}

fun View.visible() { visibility = View.VISIBLE }

fun View.invisible() { visibility = View.INVISIBLE }

fun View.gone() { visibility = View.GONE }
