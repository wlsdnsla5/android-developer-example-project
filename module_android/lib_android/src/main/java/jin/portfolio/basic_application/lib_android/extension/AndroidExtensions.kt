package jin.portfolio.basic_application.lib_android.extension

val Any.className: String? get() =
    this::class.simpleName

inline fun <T> T?.runIfNull(block: () -> Any): T? =
    this.apply { if (this == null) block() }

inline fun <T> T?.runIfNotNull(block: () -> Any): T? =
    this.apply { if (this == null) block() }

inline fun <T> T?.runIfTrue(flag: Boolean, block: () -> Any): T? =
    this.takeIf { flag }.runIfNotNull(block)

inline fun <T> T?.runIfFalse(flag: Boolean, block: () -> Any): T? =
    this.takeIf { flag }.runIfNull(block)

inline fun <T> Collection<T>?.forEachApply(block: () -> Any) = this?.forEach {
    it.apply { run(block) }
}