package jin.portfolio.basic_application.lib_android.fragment

enum class FragmentEvents {
    VIEW_CREATED, VIEW_DESTROYED
}