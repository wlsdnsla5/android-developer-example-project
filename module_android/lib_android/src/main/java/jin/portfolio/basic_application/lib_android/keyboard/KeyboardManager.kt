package jin.portfolio.basic_application.lib_android.keyboard

import androidx.lifecycle.MutableLiveData

interface KeyboardManager {
    fun getKeyboardVisibility(): MutableLiveData<KeyboardVisibility>
    fun getKeyboardType(): MutableLiveData<KeyboardType>
    fun changeKeyboardVisibility(visibility: KeyboardVisibility)
    fun changeKeyboardType(inputType: KeyboardType)
}