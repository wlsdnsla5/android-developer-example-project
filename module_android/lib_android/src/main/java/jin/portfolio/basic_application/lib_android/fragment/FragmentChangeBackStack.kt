package jin.portfolio.basic_application.lib_android.fragment

enum class FragmentChangeBackStack {
    CLEAR_ALL, CLEAR_CURRENT, REPLACE
}