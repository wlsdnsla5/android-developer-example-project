package jin.portfolio.basic_application.lib_android.fragment

/**
 * PUBLIC : logging is allowed
 * PROCESS : logging and navigation action is disabled
 * TIME_OUT : logging is allowed
 * */


enum class FragmentMode {
    PUBLIC_PROCESS,
    HIDDEN_PROCESS,
    TIME_OUT_PROCESS
}