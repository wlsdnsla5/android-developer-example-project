package jin.portfolio.basic_application.lib_android.activity

import jin.portfolio.basic_application.lib_android.menu.BasicMenuState
import jin.portfolio.basic_application.lib_android.toolbar.ToolbarState

data class BasicActivityEntity(
    val isAllowedUser: Boolean = false,
    val menuState: BasicMenuState? = null,
    val toolbarState: ToolbarState? = null
)