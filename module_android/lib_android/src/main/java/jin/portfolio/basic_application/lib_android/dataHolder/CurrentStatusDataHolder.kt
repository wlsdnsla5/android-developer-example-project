package jin.portfolio.basic_application.lib_android.dataHolder

class CurrentStatusDataHolder : DataHolder<Boolean>() {
    override fun get(tag: String) = dataHolder[tag]

    override fun put(tag: String, value: Boolean) {
        dataHolder[tag] = value
    }
}