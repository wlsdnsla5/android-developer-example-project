package jin.portfolio.basic_application.lib_android.menu

data class BasicMenuState(
    val menuLocation: BasicMenuLocation,
    val menuIconResId: Int,
    val menuActionMap: Map<Int, () -> Unit>
)