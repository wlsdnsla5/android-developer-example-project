package jin.portfolio.basic_application.lib_android.activity

import androidx.lifecycle.MutableLiveData
import io.reactivex.rxjava3.core.Scheduler
import jin.portfolio.basic_application.domain.copycat.Copycat
import jin.portfolio.basic_application.lib_android.event.FragmentChangeEvent
import jin.portfolio.basic_application.lib_android.fragment.FragmentChange
import jin.portfolio.basic_application.lib_android.fragment.FragmentResult
import jin.portfolio.basic_application.lib_android.viewModel.AndroidViewModel
import java.util.*

class BasicActivityViewModel : AndroidViewModel() {
    val activityEntityLiveData: MutableLiveData<BasicActivityEntity> = MutableLiveData()
    val fragmentChangeLiveData: MutableLiveData<FragmentChange> = MutableLiveData()
    val fragmentResultLiveData: MutableLiveData<FragmentResult> = MutableLiveData()

    val liveInformationMessage: MutableLiveData<String> = MutableLiveData()
    private val fragmentChangeLog: MutableList<FragmentChangeEvent> = mutableListOf()

    fun scheduleTimer() {
        // todo add functionality
    }

    fun changeFragment(fragmentChange: FragmentChange) {
        FragmentChangeEvent.create(fragmentChange)?.let { fragmentChangeLog.add(it) }
    }

    fun writeActivityLog() {
        fragmentChangeLog.forEach { Copycat.d(it.toString()) }

    }
}