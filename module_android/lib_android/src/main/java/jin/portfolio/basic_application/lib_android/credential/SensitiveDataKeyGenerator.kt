package jin.portfolio.basic_application.lib_android.credential

import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import java.util.*

@KoinApiExtension
class SensitiveDataKeyGenerator : KoinComponent {

    fun generateKey() = SensitiveDataKey(String(generateRandomKey()))

    private fun generateRandomKey(): ByteArray {
        return UUID.randomUUID().toString().toByteArray()
    }
}