package jin.portfolio.basic_application.lib_android.fragment

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
sealed class FragmentResult(
    val requestCode: FragmentRequestCode,
    val data: Parcelable? = null
)