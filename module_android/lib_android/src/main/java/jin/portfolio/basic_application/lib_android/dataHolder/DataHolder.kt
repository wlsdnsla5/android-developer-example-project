package jin.portfolio.basic_application.lib_android.dataHolder

sealed class DataHolder<T: Any>() {
    protected val dataHolder: HashMap<String, T> = HashMap()
    abstract fun get(tag: String): T?
    abstract fun put(tag: String, value: T)
}
