package jin.portfolio.basic_application.lib_android.menu

enum class BasicMenuLocation {
    TOP_LEFT,
    TOP_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_RIGHT,
    NONE,
}