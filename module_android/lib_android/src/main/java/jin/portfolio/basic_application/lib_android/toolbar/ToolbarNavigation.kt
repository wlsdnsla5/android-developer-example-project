package jin.portfolio.basic_application.lib_android.toolbar

enum class ToolbarNavigation {
    BACK, HOME , EXIT, NONE
}