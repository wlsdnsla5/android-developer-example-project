package jin.portfolio.basic_application.lib_android.orientation

enum class Orientation {
    PORTRAIT, LANDSCAPE
}