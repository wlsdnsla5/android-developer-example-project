package jin.portfolio.basic_application.lib_android.credential

import android.os.Parcelable
import jin.portfolio.basic_application.lib_android.data.Bytes
import jin.portfolio.basic_application.lib_android.data.SensitiveString
import org.threeten.bp.LocalDateTime

class SensitiveDataHolder {
    private val secretStringMap: MutableMap<SensitiveDataKey, String> = mutableMapOf()
    private val secretIntMap: MutableMap<SensitiveDataKey, Int> = mutableMapOf()
    private val secretDoubleMap: MutableMap<SensitiveDataKey, Double> = mutableMapOf()
    private val secretLongMap: MutableMap<SensitiveDataKey, Long> = mutableMapOf()
    private val secretLocalDateTimeMap: MutableMap<SensitiveDataKey, LocalDateTime> = mutableMapOf()
    private val secretBytesMap: MutableMap<SensitiveDataKey, Bytes> = mutableMapOf()
    private val secretParcelableMap: MutableMap<SensitiveDataKey, Parcelable> = mutableMapOf()
    private val secretSensitiveStringMap: MutableMap<SensitiveDataKey, SensitiveString> = mutableMapOf()

    fun put(key: SensitiveDataKey, value: Any) {
        when (value) {
            is String -> secretStringMap[key] = value
            is Int -> secretIntMap[key] = value
            is Double -> secretDoubleMap[key] = value
            is Long -> secretLongMap[key] = value
            is LocalDateTime -> secretLocalDateTimeMap[key] = value
            is Bytes -> secretBytesMap[key] = value
            is Parcelable -> secretParcelableMap[key] = value
            is SensitiveString -> secretSensitiveStringMap[key] = value
        }
    }
}