package jin.portfolio.basic_application.lib_android.extension

import jin.portfolio.basic_application.lib_android.viewModel.AndroidViewModel

fun AndroidViewModel.run(block: (AndroidViewModel) -> Unit) { block(this) }

fun AndroidViewModel.runOnce(block: (AndroidViewModel) -> Unit) {
    if (!isInitialized) {
        block(this)
        isInitialized = true
    }
}