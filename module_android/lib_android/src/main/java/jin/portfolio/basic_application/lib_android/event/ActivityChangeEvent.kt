package jin.portfolio.basic_application.lib_android.event

import jin.portfolio.basic_application.lib_android.extension.toLogString
import org.threeten.bp.LocalDateTime

/**
 * FragmentChangeEvent
 *
 * To log all the behavior from application
 *  Opened fragment name
 *  Opened Date and Time
 * */
data class ActivityChangeEvent(val activityName: String, val date: LocalDateTime) {
    override fun toString() = "-> Change to $activityName at ${date.toLogString()}"

    companion object {
        fun create() {

        }
    }
}