package jin.portfolio.basic_application.lib_android.fragment

enum class FragmentRequestCode {
    RESET,
    SUCCESS,
    CLEAR_BACKSTACK,
    ERROR;

    companion object {
        fun valueOf(value: String) = values().find { it.name == value }
    }
}