package jin.portfolio.basic_application.lib_menu

import android.view.Menu
import android.view.MenuInflater

data class OptionMenu(val menuId: Int)

fun OptionMenu.inflate(menu: Menu, inflater: MenuInflater) { inflater.inflate(menuId, menu) }

