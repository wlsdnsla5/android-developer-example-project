package jin.portfolio.basic_application.bluetooth.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BluetoothDevice(
    val name: String,
    val type: BluetoothDeviceType
) : Parcelable
