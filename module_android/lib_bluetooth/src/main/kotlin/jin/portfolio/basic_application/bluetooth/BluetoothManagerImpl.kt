package jin.portfolio.basic_application.bluetooth

import android.bluetooth.BluetoothAdapter
import io.reactivex.rxjava3.core.Single
import jin.portfolio.basic_application.bluetooth.model.BluetoothManager

class BluetoothManagerImpl(
    private val bluetoothEnabledBroadcastReceiver: BluetoothEnabledBroadcastReceiver,
    private val bluetoothConnectBroadcastReceiver: BluetoothEnabledBroadcastReceiver
) : BluetoothManager {
    override fun enableBluetooth(): Single<Unit> =
        bluetoothEnabledBroadcastReceiver.enable().doOnSubscribe {
            if (BluetoothAdapter.getDefaultAdapter()?.enable() != true) throw Exception()
        }

    override fun disableBluetooth() {
        TODO("Not yet implemented")
    }

    override fun connectDevice(device: jin.portfolio.basic_application.bluetooth.model.BluetoothDevice) {
        TODO("Not yet implemented")
    }

    override fun isBluetoothEnabled(): Boolean {
        TODO("Not yet implemented")
    }
}