package jin.portfolio.basic_application.bluetooth.model

import io.reactivex.rxjava3.core.Single

interface BluetoothManager {
    fun enableBluetooth(): Single<Unit>
    fun disableBluetooth()
    fun connectDevice(device: BluetoothDevice)
    fun isBluetoothEnabled(): Boolean
}