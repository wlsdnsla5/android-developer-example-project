package jin.portfolio.basic_application.bluetooth.model

enum class BluetoothDeviceType {
    SMARTPHONE, TABLET_PC, PC, WIRELESS_EARPHONE, SMART_WATCH
}