package jin.portfolio.basic_application.bluetooth

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Handler
import android.os.Looper
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class BluetoothEnabledBroadcastReceiver(private val context: Context) : BroadcastReceiver() {
    private var isBluetoothEnabled: Boolean = false

    override fun onReceive(context: Context?, intent: Intent?) {
        when (intent?.extras?.getInt(BluetoothAdapter.EXTRA_STATE)) {
            BluetoothAdapter.STATE_ON -> isBluetoothEnabled = true
            BluetoothAdapter.STATE_OFF -> isBluetoothEnabled = false
        }
    }

    fun enable(): Single<Unit> {
        registerReceiver()
        return Single.fromCallable {
            if (!isBluetoothEnabled) throw IllegalStateException()
        }
            .subscribeOn(Schedulers.io())
            .retryWhen { errors ->
                errors.flatMap { error ->
                if (error !is IllegalStateException)
                    Flowable.timer(10, TimeUnit.MILLISECONDS)
                    else throw error
                }
            }
            .doFinally { unRegisterReceiver() }
    }

    private fun registerReceiver() {
        isBluetoothEnabled = false
        Handler(Looper.getMainLooper()).post {
            context.registerReceiver(this, getBluetoothScanFilter())
        }
    }

    private fun unRegisterReceiver() {
        isBluetoothEnabled = false
        Handler(Looper.getMainLooper()).post {
            context.unregisterReceiver(this)
        }
    }

    // TODO [add more] filter after testing
    private fun getBluetoothScanFilter() = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
}