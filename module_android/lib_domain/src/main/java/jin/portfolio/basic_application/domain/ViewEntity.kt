package jin.portfolio.basic_application.domain

import android.os.Parcelable
import androidx.annotation.ColorRes
import kotlinx.android.parcel.Parcelize

sealed class ViewEntity : Parcelable {
    abstract val text: String
}

@Parcelize
data class TextViewEntity(
    override val text: String,
    val iconID: Int,
    @ColorRes val textColor: Int
) : ViewEntity()

@Parcelize
data class ImageViewEntity(
    override val text: String,
    val textColor: Int
) : ViewEntity()

@Parcelize
data class ImageButtonViewEntity(
    override val text: String,
    val iconID: Int,
    @ColorRes val textColor: Int
) : ViewEntity()
