package jin.portfolio.basic_application.domain.copycat


private const val VERBOSE = " [VERBOSE] : "
private const val DEBUG = "[DEBUG] : "
private const val INFORMATION = " [INFORMATION] : "
private const val WARNING = " [WARNING] : "
private const val ERROR = " [ERROR] : "

class CopycatImpl : CopycatInterface{
    override fun v(message: String, vararg args: Any) {
        print(VERBOSE)
        printLog(message, args)
    }

    override fun v(throwable: Throwable, message: String, vararg args: Any) {
        print(VERBOSE)
        printLog(message, args)
        println(throwable)
    }

    override fun v(throwable: Throwable) {
        print(VERBOSE)
        println(throwable)
    }

    override fun d(message: String, vararg args: Any) {
        print(DEBUG)
        printLog(message, args)
    }

    override fun d(throwable: Throwable, message: String, vararg args: Any) {
        print(DEBUG)
        printLog(message, args)
        println(throwable)
    }

    override fun d(throwable: Throwable) {
        print(DEBUG)
        println(throwable)
    }

    override fun i(message: String, vararg args: Any) {
        print(INFORMATION)
        printLog(message, args)
    }

    override fun i(throwable: Throwable, message: String, vararg args: Any) {
        print(INFORMATION)
        printLog(message, args)
        println(throwable)
    }

    override fun i(throwable: Throwable) {
        print(INFORMATION)
        println(throwable)
    }

    override fun w(message: String, vararg args: Any) {
        print(WARNING)
        printLog(message, args)
    }

    override fun w(throwable: Throwable, message: String, vararg args: Any) {
        print(WARNING)
        printLog(message, args)
        println(throwable)
    }

    override fun w(throwable: Throwable) {
        print(WARNING)
        println(throwable)
    }

    override fun e(message: String, vararg args: Any) {
        print(ERROR)
        printLog(message, args)
    }

    override fun e(throwable: Throwable, message: String, vararg args: Any) {
        print(ERROR)
        printLog(message, args)
        println(throwable)
    }

    override fun e(throwable: Throwable) {
        print(ERROR)
        println(throwable)
    }

    private fun printLog(message: String, vararg args: Any) {
        println(args.takeIf { it.isNotEmpty() }?.let { String.format(message, args) } ?: message)
    }
}