package jin.portfolio.basic_application.domain.copycat

class Copycat {
    companion object : CopycatInterface  {
        private val logger: CopycatInterface = CopycatImpl()

        override fun v(message: String, vararg args: Any) =
            logger.v(message, args)
        override fun v(throwable: Throwable, message: String, vararg args: Any) =
            logger.v(throwable, message, args)
        override fun v(throwable: Throwable) =
            logger.v(throwable)

        override fun d(message: String, vararg args: Any)  =
            logger.d(message, args)
        override fun d(throwable: Throwable, message: String, vararg args: Any)  =
            logger.d(throwable, message, args)
        override fun d(throwable: Throwable)  =
            logger.d(throwable)

        override fun i(message: String, vararg args: Any) =
            logger.i(message, args)
        override fun i(throwable: Throwable, message: String, vararg args: Any) =
            logger.i(throwable, message, args)
        override fun i(throwable: Throwable) =
            logger.i(throwable)

        override fun w(message: String, vararg args: Any) =
            logger.w(message, args)
        override fun w(throwable: Throwable, message: String, vararg args: Any) =
            logger.w(throwable, message, args)
        override fun w(throwable: Throwable) =
            logger.w(throwable)

        override fun e(message: String, vararg args: Any) =
            logger.e(message, args)
        override fun e(throwable: Throwable, message: String, vararg args: Any)  =
            logger.e(throwable, message, args)
        override fun e(throwable: Throwable) =
            logger.e(throwable)
    }
}