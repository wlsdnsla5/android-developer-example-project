package jin.portfolio.basic_application.domain

import android.content.Context
import android.os.Parcelable
import androidx.annotation.StringRes
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent

@KoinApiExtension
sealed class Label : Parcelable, KoinComponent {
    protected abstract fun materializeString(context: Context): String

    abstract fun isNotNullOrEmpty(): Boolean

    fun materialize(context: Context) = materializeString(context)

    companion object {
        fun create(string: String): Label = StringLabel(string)

        fun create(@StringRes resId: Int, vararg params: Any): Label = ResourceLabel(resId, *params)
    }
}

@KoinApiExtension
@Parcelize
private class StringLabel(private val value: String) : Label() {
    override fun materializeString(context: Context) = value

    override fun isNotNullOrEmpty(): Boolean = value.isNotEmpty()

    override fun equals(other: Any?): Boolean = other is StringLabel && value == other.value

    override fun hashCode(): Int = value.hashCode()
}

@KoinApiExtension
@Parcelize
private class ResourceLabel(
    private val resId: Int,
    private vararg val params: @RawValue Any
) : Label() {

    override fun materializeString(context: Context) =
        if (params.isEmpty()) context.getString(resId) else context.getString(resId, *params)
    override fun isNotNullOrEmpty(): Boolean = true

    override fun equals(other: Any?): Boolean =
        other is ResourceLabel && resId == other.resId && params.contentEquals(other.params)
    override fun hashCode(): Int = 100 * resId.hashCode() + params.contentHashCode()
}
